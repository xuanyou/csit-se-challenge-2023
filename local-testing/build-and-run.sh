#!/bin/bash

pushd ../solution-go
docker stop csit-se-challenge-solution-go
docker rm csit-se-challenge-solution-go
docker build -t csit-se-challenge-solution-go .
docker run -d --name csit-se-challenge-solution-go -p 8080:8080 csit-se-challenge-solution-go
popd
