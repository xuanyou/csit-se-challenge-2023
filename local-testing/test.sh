#!/bin/bash

curl "localhost:8080/flight?departureDate=2023-12-10&returnDate=2023-12-16&destination=Frankfurt"
curl "localhost:8080/hotel?checkInDate=2023-12-10&checkOutDate=2023-12-16&destination=Frankfurt"
curl "localhost:8080/flight?departureDate=2024-12-10&returnDate=2024-12-16&destination=Frankfurt"
curl "localhost:8080/hotel?checkInDate=2024-12-10&checkOutDate=2024-12-16&destination=Frankfurt"