# csit-se-challenge-2023

## Objective

- Complete the [challenge](https://go.gov.sg/csit-se-challenge-2023).
- Learn Go
- Learn Ruby

## Approach

1. Test-driven development: Write test scripts to curl the two endpoints `/flight` and `/hotel`
2. Continuous integration: Write ci scripts that build

## References
- DigitalOcean's tutorial: [How to make a HTTP server in Go](https://www.digitalocean.com/community/tutorials/how-to-make-an-http-server-in-go)
- DigitalOcean's tutorial: [How to use JSON in Go](https://www.digitalocean.com/community/tutorials/how-to-use-json-in-go)
- Gitlab's tutorial: [Build and push container images to the Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html)
- MongoDB's tutorial: [Fundamentals - Connection Guide](https://www.mongodb.com/docs/drivers/go/current/fundamentals/connection/)