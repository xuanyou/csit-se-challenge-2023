package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
	
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoDB related globals and functions

const global_db_uri = "mongodb+srv://userReadOnly:7ZT817O8ejDfhnBM@minichallenge.q4nve1r.mongodb.net/"
const global_db_name = "minichallenge"
var global_db *mongo.Client

func connectToDb() {

	// Use the SetServerAPIOptions() method to set the Stable API version to 1
	serverAPI := options.ServerAPI(options.ServerAPIVersion1)
	opts := options.Client().ApplyURI(global_db_uri).SetServerAPIOptions(serverAPI)
	// Create a new client and connect to the server
	var err error
	global_db, err = mongo.Connect(context.TODO(), opts)
	if err != nil {
		panic(err)
	}
	fmt.Println("Connected to db")
}

func disconnectFromDb() {
	err := global_db.Disconnect(context.TODO())
	if err != nil {
		panic(err)
	}
	fmt.Println("Disconnected from db")
}

// Flight related functions

type Flight struct {
	Date				time.Time	`bson:"date"`
	SrcCity			string		`bson:"srccity"`
	DestCity		string		`bson:"destcity"`
	AirlineName	string		`bson:"airlinename"`
	Price				int32			`bson:"price"`
}

func getLowestCostFlightFromDb(srccity string, destcity string, date string) (Flight, error) {
	// Set up the "flights" collection
	collection := global_db.Database(global_db_name).Collection("flights")
		
	// Make FindOne return the lowest price flight (sort by price ascending and return first row)
	opts := options.FindOne().SetSort(bson.D{{"price", 1}})

	// Make the string searches case-insensitive
	collation := &options.Collation{Locale: "en", Strength: 2}
	opts = opts.SetCollation(collation)

	// Prepare the filter bson
	dateObject, _ := time.Parse("2006-01-02", date)
	filter := bson.D {
		{"date", dateObject}, {"srccity", srccity}, {"destcity", destcity},
	}
	
	var result Flight
	err := collection.FindOne(context.TODO(), filter, opts).Decode(&result)
	if err != nil && err != mongo.ErrNoDocuments {
		panic(err)
	}
	fmt.Println(result)
	return result, err
}

func getItineraryJson(outbound Flight, inbound Flight) string {
	arrayData := make([]map[string]interface{}, 0, 5)
	itineraryData := map[string]interface{}{
		"City": outbound.DestCity,
		"Departure Date": outbound.Date.Format("2006-01-02"),
		"Departure Airline": outbound.AirlineName,
		"Departure Price": outbound.Price,
		"Return Date": inbound.Date.Format("2006-01-02"),
		"Return Airline": inbound.AirlineName,
		"Return Price": inbound.Price,
	}
	arrayData = append(arrayData, itineraryData)
	jsondata,err := json.Marshal(arrayData)
	if err != nil {
		fmt.Println("could not marshal json:", err)
		return ""
	}
	return string(jsondata)
}

func getFlight(w http.ResponseWriter, r *http.Request) {
	// Read the three query parameters, if available
	departureDate := r.URL.Query().Get("departureDate")
	returnDate := r.URL.Query().Get("returnDate")
	destination := r.URL.Query().Get("destination")

	// Reply with HTTP error if any parameter is missing
	if (departureDate == "") || (returnDate == "") || (destination == "") {
		w.WriteHeader(http.StatusBadRequest) // 400
		io.WriteString(w, "Missing parameter. departureDate, returnDate, destination must all be present.")
	}

	fmt.Println("Checking flights to", destination, "departing", departureDate, "and returning", returnDate)
	outbound, err1 := getLowestCostFlightFromDb("Singapore", destination, departureDate)
	inbound, err2 := getLowestCostFlightFromDb(destination, "Singapore", returnDate)
	if (err1 == mongo.ErrNoDocuments) || (err2 == mongo.ErrNoDocuments) {
		io.WriteString(w, "[]") // return empty array when no documents
	} else {
		io.WriteString(w, getItineraryJson(outbound, inbound))
	}
}

// Hotel related functions
type Hotel struct {
	City					string		`bson:"city"`
	CheckInDate		time.Time	`bson:"checkInDate"`
	CheckOutDate	time.Time	`bson:"checkOutDate"`
	Hotel					string		`bson:"_id"`
	Price					int32			`bson:"price"`
}

func getLowestCostHotelFromDb(checkInDate string, checkOutDate string, city string) (Hotel, error) {
	// Set up the "hotels" collection
	collection := global_db.Database(global_db_name).Collection("hotels")
	
	// Make the string searches case-insensitive
	collation := &options.Collation{Locale: "en", Strength: 2}
	opts := options.Aggregate().SetCollation(collation)

	// Prepare the pipeline stages
	// 1: Match days between checkInDate and checkOutDate inclusive, in the same city
	// 2: Group by hotel name, sum the price. Also include checkInDate, checkOutDate, and city.
	// 3: Sort by price ascending
	// 4: Limit first one
	checkInDateObject, _ := time.Parse("2006-01-02", checkInDate)
	checkOutDateObject, _ := time.Parse("2006-01-02", checkOutDate)
	matchStage := bson.D {{ "$match",
		bson.D {{ "$and",
			bson.A {
				bson.D {{"date", bson.D{{"$gte", checkInDateObject}}}},
				bson.D {{"date", bson.D{{ "$lte", checkOutDateObject}}}},
				bson.D {{"city", city }},
			},
		}},
	}}
	groupStage := bson.D {{ "$group",
		bson.D {
			{"_id", "$hotelName"},
			{"city", bson.D{{"$min", "$city"}}},
			{"checkInDate", bson.D{{"$min", "$date"}}},
			{"checkOutDate", bson.D{{"$max", "$date"}}},
			{"price", bson.D{{"$sum", "$price"}}},
		},
 	}}
	sortStage := bson.D{{"$sort", bson.D{{"price", 1}}}}
	limitStage := bson.D{{"$limit", 1}}
	
	cursor, err := collection.Aggregate(context.TODO(), mongo.Pipeline{matchStage,groupStage,sortStage,limitStage}, opts)
	if err != nil && err != mongo.ErrNoDocuments {
		panic(err)
	}
	
	var results []Hotel
	err = cursor.All(context.TODO(), &results)
	if err != nil && err != mongo.ErrNoDocuments {
    panic(err)
	}

	if len(results)	== 0 {
		var r Hotel
		return r, mongo.ErrNoDocuments
	} else {
		return results[0], err
	}
	
}

func getHotelJson(hotel Hotel) string {
	arrayData := make([]map[string]interface{}, 0, 5)
	itineraryData := map[string]interface{}{
		"City": hotel.City,
		"Check In Date": hotel.CheckInDate.Format("2006-01-02"),
		"Check Out Date": hotel.CheckOutDate.Format("2006-01-02"),
		"Hotel": hotel.Hotel,
		"Price": hotel.Price,
	}
	arrayData = append(arrayData, itineraryData)
	jsondata,err := json.Marshal(arrayData)
	if err != nil {
		fmt.Println("could not marshal json:", err)
		return ""
	}
	return string(jsondata)
}


func getHotel(w http.ResponseWriter, r *http.Request) {
	// Read the three query parameters, if available
	checkInDate := r.URL.Query().Get("checkInDate")
	checkOutDate := r.URL.Query().Get("checkOutDate")
	destination := r.URL.Query().Get("destination")

	// Reply with HTTP error if any parameter is missing
	if (checkInDate == "") || (checkOutDate == "") || (destination == "") {
		w.WriteHeader(http.StatusBadRequest) // 400
		io.WriteString(w, "Missing parameter. checkInDate, checkOutDate, destination must all be present.")
	}

	fmt.Println("Checking hotels at", destination, "from", checkInDate, "to", checkOutDate)
	hotel, err := getLowestCostHotelFromDb(checkInDate, checkOutDate, destination)
	if err == mongo.ErrNoDocuments {
		io.WriteString(w, "[]") // return empty array when no documents
	} else {
		io.WriteString(w, getHotelJson(hotel))
	}
	
}

// MAIN SERVER

func main() {
	connectToDb()
	defer disconnectFromDb()

	http.HandleFunc("/flight", getFlight)
	http.HandleFunc("/hotel", getHotel)

	err := http.ListenAndServe(":8080", nil)
	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error starting server: %s\n", err)
		os.Exit(1)
	}
}